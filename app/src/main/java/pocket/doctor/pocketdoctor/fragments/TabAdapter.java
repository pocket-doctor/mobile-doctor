package pocket.doctor.pocketdoctor.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jun on 11/8/16.
 * You can't understand this, Unless you're a genius!
 */
public class TabAdapter extends FragmentPagerAdapter {
    private final List<Fragment> pages = new ArrayList<>();
    private final List<String> tabs = new ArrayList<>();

    public TabAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        return pages.get(position);
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    public void addPage(Fragment fragment, String title) {
        pages.add(fragment);
        tabs.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs.get(position);
    }
}