package pocket.doctor.pocketdoctor.fragments;

public enum FragmentKeys {
    DOCTORS_FRAGMENT_TAG("doctors.DoctorFragment.TAG"),
    DOCTORS_SCHEDULE_ID_TAG("schedule.Schedule.id.TAG"),
    DOCTORS_SCHEDULE_DAY_DATE_TAG("schedule.schedule.dates.TAG"),
    RESERVATION_TYPE_ID_TAG("reservation.ReservationType.index.TAG"),
    SHOW_PROFILE_TAG("profile.ProfileFragment.TAG");

    private final static String PACKAGE = "pocket.doctor.pocketdoctor";

    private final String key;

    public String getKey() {
        return String.format("%s.%s", PACKAGE, key);
    }

    FragmentKeys(String key) {
        this.key = key;
    }
}