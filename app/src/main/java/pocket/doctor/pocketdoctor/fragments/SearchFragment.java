package pocket.doctor.pocketdoctor.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.doctors.DoctorFragment;

public class SearchFragment extends Fragment {

    public SearchFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        getChildFragmentManager().beginTransaction()
                .replace(R.id.items, new DoctorFragment(), FragmentKeys.DOCTORS_FRAGMENT_TAG.getKey())
                .commit();
        initButtons(view);
    }

    private void initButtons(final View root) {
        ImageButton button = (ImageButton) root.findViewById(R.id.search_button);
        if (button != null) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchQuery(root);
                }
            });
        }
    }

    private void searchQuery(final View root) {
        EditText value = (EditText) root.findViewById(R.id.search_box);
        DoctorFragment fragment = (DoctorFragment) getChildFragmentManager()
                .findFragmentByTag(FragmentKeys.DOCTORS_FRAGMENT_TAG.getKey());
        String query = value.getText().toString();
        fragment.search(query);
//        printLocation();
    }
}
