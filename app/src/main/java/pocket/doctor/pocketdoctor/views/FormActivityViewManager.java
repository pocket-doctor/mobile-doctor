package pocket.doctor.pocketdoctor.views;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

/**
 * Created by jun on 1/4/17.
 * You can't understand this, Unless you're a genius!
 */
public class FormActivityViewManager extends ViewManager {
    private final Activity activity;

    @Override
    public View findViewById(int id) {
        return activity.findViewById(id);
    }

    private Intent getIntent() {
        return activity.getIntent();
    }

    public FormActivityViewManager(Activity activity) {
        this.activity = activity;
    }


    public void setTextFromKeyInIntent(String key, Integer id) {
        if (getIntent() != null && getIntent().getExtras() != null)
            setTextOfId(getIntent().getExtras().getString(key, null), id);
    }

    public void startActivity(Class<?> cls) {
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);
//        finish();
    }

}
