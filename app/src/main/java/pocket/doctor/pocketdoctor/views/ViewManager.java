package pocket.doctor.pocketdoctor.views;

import android.view.View;
import android.widget.TextView;

/**
 * Created by jun on 1/7/17.
 * You can't understand this, Unless you're a genius!
 */
public abstract class ViewManager {
    public abstract View findViewById(int id);

    public String getTextOfId(Integer id) {
        final TextView textView = (TextView) findViewById(id);
        if (textView != null)
            return textView.getText().toString();
        return null;
    }

    public void setTextOfId(String value, Integer id) {
        final TextView textView = (TextView) findViewById(id);
        if (value != null && textView != null)
            textView.setText(value);
    }

    public void setOnClickListenerForId(View.OnClickListener onClickListener, Integer id) {
        final View button = findViewById(id);
        if (button != null)
            button.setOnClickListener(onClickListener);
    }
}
