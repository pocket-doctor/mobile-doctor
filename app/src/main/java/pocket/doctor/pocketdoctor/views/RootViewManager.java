package pocket.doctor.pocketdoctor.views;

import android.view.View;

/**
 * Created by jun on 1/7/17.
 * You can't understand this, Unless you're a genius!
 */
public class RootViewManager extends ViewManager {
    private final View root;

    public RootViewManager(View root) {
        this.root = root;
    }

    @Override
    public View findViewById(int id) {
        return root.findViewById(id);
    }
}
