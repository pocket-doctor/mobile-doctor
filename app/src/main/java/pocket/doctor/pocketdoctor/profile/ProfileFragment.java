package pocket.doctor.pocketdoctor.profile;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.user.User;
import pocket.doctor.pocketdoctor.views.RootViewManager;

public class ProfileFragment extends Fragment implements OnMapReadyCallback {

    private OnEditProfileFragmentActionListener mListener;
    private ProfileGetter getter;
    private RootViewManager manager;

    public ProfileFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        manager = new RootViewManager(view);
        initValues();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void download() {
        if (getter == null)
            getter = new ProfileGetter() {
                @Override
                public void onReceived(User content) {
                    showMyProfile(content);
                }

                @Override
                public void onFail(Throwable t) {
                    getter.getMyProfile();
                }
            };
        getter.getMyProfile();
    }

    private void initValues() {
        download();
        manager.setOnClickListenerForId(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditButtonPressed();
            }
        }, R.id.edit_profile);
    }


    @Override
    public void onResume() {
        super.onResume();
        download();
    }

    private void showMyProfile(User user) {
        manager.setTextOfId(user.getFirstName(), R.id.first_name);
        manager.setTextOfId(user.getLastName(), R.id.last_name);
        manager.setTextOfId(user.getEmail(), R.id.email);
        manager.setTextOfId(user.getMelliCode(), R.id.national_id);
        manager.setTextOfId(user.getPhoneNumber(), R.id.phone_number);
    }


    public void onEditButtonPressed() {
        if (mListener != null)
            mListener.onEditClicked();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEditProfileFragmentActionListener) {
            mListener = (OnEditProfileFragmentActionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEditProfileFragmentActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnEditProfileFragmentActionListener {
        void onEditClicked();
    }
}
