package pocket.doctor.pocketdoctor.profile;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.net.ServiceGetter;
import pocket.doctor.pocketdoctor.tools.Settings;
import pocket.doctor.pocketdoctor.user.User;

/**
 * Created by jun on 12/23/16.
 * You can't understand this, Unless you're a genius!
 */
public abstract class ProfilePoster extends ServiceGetter<User> {
    public void editProfile(User editedUser) {
        ServiceGenerator.getInstance().createService(ProfileAPI.class)
                .editMyProfile(Settings.getInstance().getAuthToken(), editedUser).enqueue(this);
    }
}
