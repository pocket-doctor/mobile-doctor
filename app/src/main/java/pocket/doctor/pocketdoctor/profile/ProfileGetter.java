package pocket.doctor.pocketdoctor.profile;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.net.ServiceGetter;
import pocket.doctor.pocketdoctor.tools.Settings;
import pocket.doctor.pocketdoctor.user.User;

/**
 * Created by jun on 12/23/16.
 * You can't understand this, Unless you're a genius!
 */
public abstract class ProfileGetter extends ServiceGetter<User> {
    public void getMyProfile() {
        ServiceGenerator.getInstance().createService(ProfileAPI.class)
                .getMyProfile(Settings.getInstance().getAuthToken()).enqueue(this);
    }
}
