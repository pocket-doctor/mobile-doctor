package pocket.doctor.pocketdoctor.profile;

import pocket.doctor.pocketdoctor.user.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by jun on 12/23/16.
 * You can't understand this, Unless you're a genius!
 */
public interface ProfileAPI {
    @GET("profile/")
    Call<User> getMyProfile(@Header("Authorization") String authToken);

    @POST("profile/")
    Call<User> editMyProfile(@Header("Authorization") String authToken, @Body User edited);

}
