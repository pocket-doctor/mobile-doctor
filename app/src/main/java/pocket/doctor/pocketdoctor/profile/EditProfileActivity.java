package pocket.doctor.pocketdoctor.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.activities.FormActivity;
import pocket.doctor.pocketdoctor.user.User;

public class EditProfileActivity extends FormActivity {
    private ProfileGetter getter;
    private ProfilePoster poster;
    private User firstUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        initValues();
        initButtons();
    }

    @Override
    protected void initButtons() {
        setOnClickListenerForId(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        }, R.id.edit_profile_button);
    }

    private void editProfile() {
        final User user = getUser();
        poster = new ProfilePoster() {
            @Override
            public void onReceived(User content) {
                if (content != null)
                    finish();
            }

            @Override
            public void onFail(Throwable t) {
                poster.editProfile(user);
            }
        };
        poster.editProfile(user);
    }

    String getIfDiffers(int id, String last) {
        final String current = getTextOfId(id);
        if (current != null && !last.equals(current))
            return current;
        return null;
    }

    private User getUser() {
        User user = new User();
        user.setFirstName(getIfDiffers(R.id.first_name, firstUser.getFirstName()));
        user.setLastName(getIfDiffers(R.id.last_name, firstUser.getLastName()));
        user.setEmail(getIfDiffers(R.id.email, firstUser.getEmail()));
        user.setPhoneNumber(getIfDiffers(R.id.phone_number, firstUser.getPhoneNumber()));
        user.setMelliCode(getIfDiffers(R.id.national_id, firstUser.getMelliCode()));
        return user;
    }

    private void initValues() {
        getter = new ProfileGetter() {
            @Override
            public void onReceived(User content) {
                showMyProfile(content);
            }

            @Override
            public void onFail(Throwable t) {
                getter.getMyProfile();
            }
        };
        getter.getMyProfile();

        final Button editProfileButton = (Button) findViewById(R.id.edit_profile);
        if (editProfileButton != null)
            editProfileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editProfile();
                }
            });
    }

    private void showMyProfile(User user) {
        firstUser = user;
        setTextOfId(user.getFirstName(), R.id.first_name);
        setTextOfId(user.getLastName(), R.id.last_name);
        setTextOfId(user.getEmail(), R.id.email);
        setTextOfId(user.getMelliCode(), R.id.national_id);
        setTextOfId(user.getPhoneNumber(), R.id.phone_number);
    }


}
