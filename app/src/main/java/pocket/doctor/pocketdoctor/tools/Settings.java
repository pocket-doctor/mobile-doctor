package pocket.doctor.pocketdoctor.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.reservation.ReservationType;

/**
 * Created by jun on 12/22/16.
 * You can't understand this, Unless you're a genius!
 */
public class Settings {
    private final static Settings INSTANCE = new Settings();
    //    private Context context;
    private String token;

    private Settings() {
    }


    public static Settings getInstance() {
        return INSTANCE;
    }

    public void setToken(String token, Context context) {
        this.token = token;
        getEditor(context).putString(context.getString(R.string.savedAuthToken), token).commit();
    }

    private Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    private SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(context.getString(R.string.settingsPreferences),
                Context.MODE_PRIVATE);
    }

    public String getToken(Context context) {
        SharedPreferences preferences = getPreferences(context);
        token = preferences.getString(context.getString(R.string.savedAuthToken), null);
        return token;
    }

    public String getToken() {
        return token;
    }

    public String getAuthToken() {
        return String.format("JWT %s", getToken());
    }

    public String getAuthToken(Context context) {
        return String.format("JWT %s", getToken(context));
    }

    public int getLastId(ReservationType type, Context context) {
        return getPreferences(context).getInt(type.name(), -1);
    }

    public void saveLastId(ReservationType type, int id, Context context) {
        getEditor(context).putInt(type.name(), id).commit();
    }
}