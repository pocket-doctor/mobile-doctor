package pocket.doctor.pocketdoctor.doctors;

/**
 * Created by jun on 2/4/17.
 * You can't understand this, Unless you're a genius!
 */
public enum DoctorType {
    ADVERTISEMENT("advertisement/search"),
    NORMAL_DOCTOR("search");

    private final String url;

    DoctorType(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
