package pocket.doctor.pocketdoctor.doctors;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.imanoweb.calendarview.CalendarListener;
import com.imanoweb.calendarview.CustomCalendarView;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.activities.ActivityKeys;
import pocket.doctor.pocketdoctor.activities.FormActivity;
import pocket.doctor.pocketdoctor.fragments.FragmentKeys;
import pocket.doctor.pocketdoctor.reservation.ReserveRequester;
import pocket.doctor.pocketdoctor.schedule.DayViewDecorator;
import pocket.doctor.pocketdoctor.schedule.Schedule;
import pocket.doctor.pocketdoctor.schedule.ScheduleDaysGetter;
import pocket.doctor.pocketdoctor.time.TimeSlot;
import pocket.doctor.pocketdoctor.time.TimeSlotFragment;

public class DoctorActivity extends FormActivity implements TimeSlotFragment.OnListFragmentInteractionListener {
    private SingleDoctorGetter doctorGetter;
    private ScheduleDaysGetter scheduleDaysGetter;
    private Doctor doctor = null;
    private Schedule schedule = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);
        final int id = getIntent().getExtras().getInt(ActivityKeys.DOCTOR_ID.getKey(), -1);
        if (id != -1)
            showDoctor(id);
        initButtons();
    }

    @Override
    protected void initButtons() {
        setOnClickListenerForId(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call();
            }
        }, R.id.call);
    }

    private void showDoctor(final int id) {
        doctorGetter = new SingleDoctorGetter() {
            @Override
            public void onReceived(Doctor content) {
                Log.d("MA", "Doctor is" + content);
                showDoctor(content);
            }

            @Override
            public void onFail(Throwable t) {
                t.printStackTrace();
                doctorGetter.getDoctor(id);
            }
        };
        doctorGetter.getDoctor(id);
    }

    private void downloadSchedule(final int id) {
        scheduleDaysGetter = new ScheduleDaysGetter() {
            @Override
            public void onReceived(Schedule schedule) {
                showSchedule(schedule);
            }

            @Override
            public void onFail(Throwable t) {
                scheduleDaysGetter.getScheduleDays(id);
            }
        };
        scheduleDaysGetter.getScheduleDays(id);
    }

    private void showSchedule(Schedule schedule) {
        this.schedule = schedule;
        initCalendar();
    }

    private void showDoctor(Doctor doctor) {
        this.doctor = doctor;
        setTextOfId(doctor.getName(), R.id.name);
        setTextOfId(doctor.getDegree(), R.id.degree);
        setTextOfId(doctor.getDegreeYear(), R.id.graduation);
        setTextOfId(doctor.getUniversity(), R.id.university);
        setTextOfId(doctor.getClinicAddress(), R.id.address);
        setTextOfId(doctor.getClinicPhone(), R.id.phone_number);

        downloadSchedule(doctor.getSchedule());
    }

    private void initCalendar() {
        final CustomCalendarView calendarView = (CustomCalendarView) findViewById(R.id.calendar);
        final Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());
        if (calendarView != null) {
            calendarView.setVisibility(View.VISIBLE);
            calendarView.setShowOverflowDate(false);
            final int active = ContextCompat.getColor(this, R.color.black);
            final int disabledColor = ContextCompat.getColor(this, R.color.disabledDate);
            calendarView.setDecorators(DayViewDecorator.getInstance(schedule, active, disabledColor));
            calendarView.refreshCalendar(currentCalendar);
            calendarView.setCalendarListener(new CalendarListener() {
                @Override
                public void onDateSelected(Date date) {
                    if (schedule.isWithinSchedule(date))
                        addFragment(date);
                    else
                        removeLast();
                }

                @Override
                public void onMonthChanged(Date date) {
                    removeLast();
                }
            });

        }
    }

    private void addFragment(Date date) {
        final TimeSlotFragment fragment = new TimeSlotFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(FragmentKeys.DOCTORS_SCHEDULE_ID_TAG.getKey(), doctor.getSchedule());
        bundle.putSerializable(FragmentKeys.DOCTORS_SCHEDULE_DAY_DATE_TAG.getKey(), date);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).commit();
        View frame = findViewById(R.id.frame);
        if (frame != null)
            frame.setVisibility(View.VISIBLE);
    }

    private void removeLast() {
        View frame = findViewById(R.id.frame);
        if (frame != null)
            frame.setVisibility(View.GONE);
    }

    private void call() {
        if (doctor != null) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + doctor.getClinicPhone()));
            startActivity(intent);
        }
    }

    @Override
    public void onListFragmentInteraction(TimeSlot item) {
        new ReserveRequester() {
            @Override
            public void onReceived(Void content) {
                showSuccess();
            }

            @Override
            public void onFail(Throwable t) {
                showFail();
            }
        }.request(item.getId());
    }

    private void showFail() {
        Toast toast = Toast.makeText(this, getString(R.string.reservation_fail_status), Toast.LENGTH_LONG);
        toast.show();
    }

    private void showSuccess() {
        Toast toast = Toast.makeText(this, getString(R.string.reservation_success_status), Toast.LENGTH_LONG);
        toast.show();
    }
}

