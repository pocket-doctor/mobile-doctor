package pocket.doctor.pocketdoctor.doctors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.doctors.DoctorFragment.OnDoctorsListFragmentClickListener;
import pocket.doctor.pocketdoctor.scroll.InfiniteScrollRecyclerViewAdapter;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Doctor} and makes a call to the
 * specified {@link DoctorFragment.OnDoctorsListFragmentClickListener}.
 */
public class MyDoctorRecyclerViewAdapter extends InfiniteScrollRecyclerViewAdapter<Doctor, MyDoctorRecyclerViewAdapter.ViewHolder> {
    public static int ADVERTISEMENT = 0;
    public static int NORMAL_DOCTOR = 1;
    int advertisement = 0;

    private final DoctorFragment.OnDoctorsListFragmentClickListener mListener;

    public MyDoctorRecyclerViewAdapter(OnDoctorsListFragmentClickListener listener) {
        mListener = listener;
    }

    public void setAdvertisement(int advertisement) {
        this.advertisement = advertisement;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < advertisement)
            return ADVERTISEMENT;
        return NORMAL_DOCTOR;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == NORMAL_DOCTOR)
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_doctor, parent, false);
        else
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_advertised_doctor, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setup(mValues.get(position));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onDoctorClicked(holder.getDoctor());
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private final TextView mName;
        private final TextView mDegree;
        private final TextView mPhoneNumber;
        private final TextView mAddress;
        private Doctor mDoctor;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mName = (TextView) view.findViewById(R.id.name);
            mDegree = (TextView) view.findViewById(R.id.degree);
            mPhoneNumber = (TextView) view.findViewById(R.id.phone_number);
            mAddress = (TextView) view.findViewById(R.id.address);
        }

        public void setup(Doctor doctor) {
            mDoctor = doctor;
            setIfNotNull(mName, doctor.getName(), "بدون نام");
            setIfNotNull(mDegree, doctor.getDegree(), "بدون تحصیلات");
            setIfNotNull(mPhoneNumber, doctor.getClinicPhone(), "بدون شماره تماس");
            setIfNotNull(mAddress, doctor.getClinicAddress(), "بدون آدرس");
        }

        private void setIfNotNull(TextView textView, String value, String defaultValue) {
            if (value == null)
                value = defaultValue;
            textView.setText(value);
        }

        public Doctor getDoctor() {
            return mDoctor;
        }
    }
}
