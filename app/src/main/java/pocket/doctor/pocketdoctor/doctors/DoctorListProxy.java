package pocket.doctor.pocketdoctor.doctors;

import pocket.doctor.pocketdoctor.scroll.InfiniteScrollAdapterAPI;
import pocket.doctor.pocketdoctor.scroll.PaginatedList;
import pocket.doctor.pocketdoctor.scroll.SearchScrollGetter;
import retrofit2.Call;

/**
 * Created by jun on 2/4/17.
 * You can't understand this, Unless you're a genius!
 */
public class DoctorListProxy extends SearchScrollGetter<String, Doctor> implements InfiniteScrollAdapterAPI<Doctor> {
    boolean downloadedAdvertisements = false;
    private final DoctorsListGetter doctorsGetter;
    private final DoctorsListGetter advertisementGetter;

    protected DoctorListProxy(MyDoctorRecyclerViewAdapter dataNeededObject) {
        super(dataNeededObject);
        doctorsGetter = new DoctorsListGetter(this, DoctorType.NORMAL_DOCTOR);
        advertisementGetter = new DoctorsListGetter(this, DoctorType.ADVERTISEMENT);
    }

    @Override
    public void cancel() {
        advertisementGetter.cancel();
        doctorsGetter.cancel();
        downloadedAdvertisements = false;
    }

    @Override
    public void setQuery(String query) {
        super.setQuery(query);
        advertisementGetter.setQuery(query);
        doctorsGetter.setQuery(query);
    }

    @Override
    public void download() {
        downloadedAdvertisements = false;
        advertisementGetter.download();
    }

    @Override
    protected Call<PaginatedList<Doctor>> getCall() {
        return null;
    }

    @Override
    public void updateDataSet(PaginatedList<Doctor> data) {
        if (!downloadedAdvertisements) {
            dataNeededObject.resetDataSet();
            doctorsGetter.download();
            downloadedAdvertisements = true;
            if (data != null && data.getResults() != null)
                ((MyDoctorRecyclerViewAdapter) dataNeededObject)
                        .setAdvertisement(data.getResults().size());
        }
        this.dataNeededObject.updateDataSet(data);
    }

    @Override
    public void resetDataSet() {
        dataNeededObject.resetDataSet();
    }
}
