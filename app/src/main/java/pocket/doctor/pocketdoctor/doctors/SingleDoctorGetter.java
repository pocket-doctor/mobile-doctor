package pocket.doctor.pocketdoctor.doctors;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.net.ServiceGetter;
import pocket.doctor.pocketdoctor.tools.Settings;

/**
 * Created by jun on 12/23/16.
 * You can't understand this, Unless you're a genius!
 */
public abstract class SingleDoctorGetter extends ServiceGetter<Doctor> {
    public void getDoctor(int id) {
        ServiceGenerator.getInstance().createService(DoctorsAPI.class)
                .getDoctor(Settings.getInstance().getAuthToken(), id).enqueue(this);
    }
}
