package pocket.doctor.pocketdoctor.doctors;

/**
 * Created by jun on 12/23/16.
 * You can't understand this, Unless you're a genius!
 */
public class Doctor {
    private int id;
    private String name;
    private String degree;
    private String degreeYear;
    private String university;
    private String clinicPhone;
    private String clinicAddress;
    private int schedule;


    public String getClinicAddress() {
        return clinicAddress;
    }

    public String getClinicPhone() {
        return clinicPhone;
    }

    public String getUniversity() {
        return university;
    }

    public String getDegreeYear() {
        return degreeYear;
    }

    public String getDegree() {
        return degree;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getSchedule() {
        return schedule;
    }
}
