package pocket.doctor.pocketdoctor.doctors;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.scroll.InfiniteScrollAdapterAPI;
import pocket.doctor.pocketdoctor.scroll.PaginatedList;
import pocket.doctor.pocketdoctor.scroll.SearchScrollGetter;
import pocket.doctor.pocketdoctor.tools.Settings;
import retrofit2.Call;

/**
 * Created by jun on 12/23/16.
 * You can't understand this, Unless you're a genius!
 */
public class DoctorsListGetter extends SearchScrollGetter<String, Doctor> {
    private final DoctorType doctorType;

    public DoctorsListGetter(InfiniteScrollAdapterAPI<Doctor> dataNeededObject, DoctorType doctorType) {
        super(dataNeededObject);
        this.doctorType = doctorType;
    }


    @Override
    public void setQuery(String query) {
        if (query.equals(""))
            query = "*";
        super.setQuery(query);
    }

    @Override
    protected Call<PaginatedList<Doctor>> getCall() {
        return ServiceGenerator.getInstance().createService(DoctorsAPI.class)
                .getDoctors(Settings.getInstance().getAuthToken(), doctorType.getUrl(), getQuery());
    }
}
