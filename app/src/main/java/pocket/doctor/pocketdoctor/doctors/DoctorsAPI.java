package pocket.doctor.pocketdoctor.doctors;

import pocket.doctor.pocketdoctor.scroll.PaginatedList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by jun on 12/23/16.
 * You can't understand this, Unless you're a genius!
 */
public interface DoctorsAPI {
    @GET("{type}/{query}/")
    Call<PaginatedList<Doctor>> getDoctors(@Header("Authorization") String authToken,
                                           @Path("type") String type, @Path("query") String Query);

    @GET("doctor/{id}/")
    Call<Doctor> getDoctor(@Header("Authorization") String authToken, @Path("id") int id);
}
