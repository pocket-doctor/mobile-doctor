package pocket.doctor.pocketdoctor.doctors;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pocket.doctor.pocketdoctor.R;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnDoctorsListFragmentClickListener}
 * interface.
 */
public class DoctorFragment extends Fragment {

    private DoctorListProxy getter;
    private OnDoctorsListFragmentClickListener mListener;
    private MyDoctorRecyclerViewAdapter adapter;

    public DoctorFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctor_list, container, false);
        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new MyDoctorRecyclerViewAdapter(mListener);
        recyclerView.setAdapter(adapter);
        getter = new DoctorListProxy(adapter);
        getter.setQuery("");
        getter.download();
        return view;
    }

    public void search(String query) {
        getter.cancel();
        getter.setQuery(query);
        getter.download();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDoctorsListFragmentClickListener) {
            mListener = (OnDoctorsListFragmentClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDoctorsListFragmentClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnDoctorsListFragmentClickListener {
        void onDoctorClicked(Doctor doctor);
    }
}
