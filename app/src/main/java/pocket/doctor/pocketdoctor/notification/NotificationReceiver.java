package pocket.doctor.pocketdoctor.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by jun on 1/9/17.
 * You can't understand this, Unless you're a genius!
 */
public class NotificationReceiver extends BroadcastReceiver {
    private final static int THRESHOLD = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, NotificationService.class));
        Log.d("GIBILI", "Service started");
    }

    public void setPollingPeriod(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, NotificationReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * THRESHOLD, pi);
    }
}
