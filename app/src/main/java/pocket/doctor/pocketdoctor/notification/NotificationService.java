package pocket.doctor.pocketdoctor.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import java.io.IOException;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.activities.MainActivity;
import pocket.doctor.pocketdoctor.reservation.ReservationGetter;
import pocket.doctor.pocketdoctor.reservation.ReservationType;
import pocket.doctor.pocketdoctor.tools.Settings;

/**
 * Created by jun on 1/9/17.
 * You can't understand this, Unless you're a genius!
 */
public class NotificationService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        runMyThread();
        return START_STICKY;
    }

    private void runMyThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Settings.getInstance().getAuthToken(NotificationService.this);
                checkForNotification(ReservationType.ACCEPTED);
                checkForNotification(ReservationType.REJECTED);

                stopSelf();
            }


        }).start();
    }

    private void checkForNotification(ReservationType type) {
        int id = download(ReservationType.ACCEPTED);
        if (id != -1 && id != Settings.getInstance().getLastId(type, this)) {
            Settings.getInstance().saveLastId(type, id, this);
            createNotification(type);
        }
    }

    private void createNotification(ReservationType type) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_menu_send)
                        .setContentTitle("One of your reservations has been " + type.name())
                        .setContentText("Please check your reservations");
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }

    private int download(ReservationType reservationType) {
        try {
            return new ReservationGetter(null, reservationType).getFirstReservationId();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
