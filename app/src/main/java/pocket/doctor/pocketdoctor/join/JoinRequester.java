package pocket.doctor.pocketdoctor.join;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.net.ServiceGetter;
import pocket.doctor.pocketdoctor.user.User;

/**
 * Created by jun on 12/21/16.
 * You can't understand this, Unless you're a genius!
 */
public abstract class JoinRequester extends ServiceGetter<User> {
    public void request(JoinRequest request) {
        ServiceGenerator.getInstance().createService(JoinAPI.class).join(request).enqueue(this);
    }
}
