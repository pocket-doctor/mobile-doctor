package pocket.doctor.pocketdoctor.join;

import pocket.doctor.pocketdoctor.user.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by jun on 12/21/16.
 * You can't understand this, Unless you're a genius!
 */
public interface JoinAPI {
    @POST("join/")
    Call<User> join(@Body JoinRequest request);
}
