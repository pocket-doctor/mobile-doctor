package pocket.doctor.pocketdoctor.join;

import pocket.doctor.pocketdoctor.user.Member;
import pocket.doctor.pocketdoctor.user.User;

/**
 * Created by jun on 12/21/16.
 * You can't understand this, Unless you're a genius!
 */
public class JoinRequest extends User {
    protected String password;
    protected String confirmPassword;

    public JoinRequest(Member member, String username, String firstName, String lastName,
                       String email, String password, String confirmPassword) {
        super(member, username, firstName, lastName, email);
        this.password = password;
        this.confirmPassword = confirmPassword;
    }
}
