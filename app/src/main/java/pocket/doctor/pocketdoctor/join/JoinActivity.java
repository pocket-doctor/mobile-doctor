package pocket.doctor.pocketdoctor.join;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.activities.ActivityKeys;
import pocket.doctor.pocketdoctor.activities.FormActivity;
import pocket.doctor.pocketdoctor.auth.LoginActivity;
import pocket.doctor.pocketdoctor.user.Member;
import pocket.doctor.pocketdoctor.user.User;

public class JoinActivity extends FormActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);

        loadPredefinedUsername();
        initButtons();
    }

    @Override
    protected void initButtons() {
        setOnClickListenerForId(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                join();
            }
        }, R.id.join_button);
    }

    private void join() {
        final Member member = new Member(getTextOfId(R.id.national_id), getTextOfId(R.id.phone_number));
        final JoinRequest request = new JoinRequest(member, getTextOfId(R.id.username),
                getTextOfId(R.id.name), getTextOfId(R.id.family), getTextOfId(R.id.email),
                getTextOfId(R.id.password), getTextOfId(R.id.password));
        final JoinRequester requester = new JoinRequester() {
            @Override
            public void onReceived(User content) {
                if (content != null)
                    login();
                else
                    printError();
            }

            @Override
            public void onFail(Throwable t) {
                Log.d("MA", "FAILED");
                t.printStackTrace();
                printError();
            }
        };
        requester.request(request);
    }

    private void printError() {
        TextView textView = (TextView) findViewById(R.id.join_error);
        if (textView != null)
            textView.setVisibility(View.VISIBLE);
    }

    private void login() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(ActivityKeys.JOIN_DEFAULT_PASSWORD.getKey(), getTextOfId(R.id.password));
        intent.putExtra(ActivityKeys.JOIN_DEFAULT_USERNAME.getKey(), getTextOfId(R.id.username));
        startActivity(intent);
    }

    private void loadPredefinedUsername() {
        setTextFromKeyInIntent(ActivityKeys.JOIN_DEFAULT_USERNAME.getKey(), R.id.username);
    }
}
