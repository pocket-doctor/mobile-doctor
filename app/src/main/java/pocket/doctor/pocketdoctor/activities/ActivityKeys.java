package pocket.doctor.pocketdoctor.activities;

/**
 * Created by jun on 12/21/16.
 * You can't understand this, Unless you're a genius!
 */
public enum ActivityKeys {
    JOIN_DEFAULT_USERNAME("activities.JoinActivity.USERNAME"),
    JOIN_DEFAULT_PASSWORD("activities.JoinActivity.PASSWORD"),
    DOCTOR_ID("activities.DoctorActivity.DOCTOR_ID"),
    ;

    private final static String PACKAGE = "pocket.doctor.pocketdoctor";

    private final String key;

    public String getKey() {
        return String.format("%s.%s", PACKAGE, key);
    }

    ActivityKeys(String key) {
        this.key = key;
    }
}
