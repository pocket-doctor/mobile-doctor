package pocket.doctor.pocketdoctor.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.advertisement.AdvertisementFragment;
import pocket.doctor.pocketdoctor.doctors.Doctor;
import pocket.doctor.pocketdoctor.doctors.DoctorActivity;
import pocket.doctor.pocketdoctor.doctors.DoctorFragment;
import pocket.doctor.pocketdoctor.doctors.SingleDoctorGetter;
import pocket.doctor.pocketdoctor.fragments.SearchFragment;
import pocket.doctor.pocketdoctor.fragments.TabAdapter;
import pocket.doctor.pocketdoctor.profile.EditProfileActivity;
import pocket.doctor.pocketdoctor.profile.ProfileFragment;
import pocket.doctor.pocketdoctor.reservation.Reservation;
import pocket.doctor.pocketdoctor.reservation.ReservationCancelerPoster;
import pocket.doctor.pocketdoctor.reservation.ReservationFragment;
import pocket.doctor.pocketdoctor.reservation.ReservationType;
import pocket.doctor.pocketdoctor.reservation.ReservationsBaseFragment;

public class MainActivity extends AppCompatActivity implements DoctorFragment.OnDoctorsListFragmentClickListener,
        ProfileFragment.OnEditProfileFragmentActionListener, ReservationFragment.OnCancelListener {

    private static final int LOCATION_REQUEST_CODE = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        syncAdapterAndPager();

        new SingleDoctorGetter() {
            @Override
            public void onReceived(Doctor content) {
                Log.d("MA", "Doctor is" + content);
            }

            @Override
            public void onFail(Throwable t) {
                Log.d("MA", "FAILED");
            }
        }.getDoctor(1);

        printLocation();
        Log.d("MA", "Started the download");
    }

    private void syncAdapterAndPager() {
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        if (pager != null && tabs != null) {
            initPager(pager);
            tabs.setupWithViewPager(pager);
        }
    }

    private void initPager(final ViewPager pager) {
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addPage(new SearchFragment(), "Search");
        adapter.addPage(new AdvertisementFragment(), "Ads");
        adapter.addPage(new ProfileFragment(), "Profile");
        adapter.addPage(new ReservationsBaseFragment(), "Notifs");
        pager.setAdapter(adapter);
    }

    @Override
    public void onDoctorClicked(Doctor doctor) {
        showDoctor(doctor);
    }

    private void showDoctor(Doctor doctor) {
        Intent intent = new Intent(this, DoctorActivity.class);
        intent.putExtra(ActivityKeys.DOCTOR_ID.getKey(), doctor.getId());
        startActivity(intent);
    }

    @Override
    public void onEditClicked() {
        Intent intent = new Intent(this, EditProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCancelClicked(final Reservation reservation, final ReservationType type) {
        ReservationCancelerPoster cancelerPoster = new ReservationCancelerPoster(type) {
            @Override
            public void onReceived(Void content) {
                Toast toast = Toast.makeText(MainActivity.this,
                        "You have successfully canceled your" + type.getCancelURL(),
                        Toast.LENGTH_LONG);
                toast.show();
            }

            @Override
            public void onFail(Throwable t) {
                this.cancel(reservation.getSlotID());
            }
        };
        cancelerPoster.cancel(reservation.getSlotID());
    }

    private void printLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
//               public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST_CODE);
            Log.d("Position", "No permission");
            Log.d("Position", "P1: " + ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) + " " + PackageManager.PERMISSION_GRANTED);
            Log.d("Position", "P1: " + ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) + " " + PackageManager.PERMISSION_GRANTED);
            return;
        }
        realPrint();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            realPrint();
        }
    }

    private void realPrint() {
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            Toast.makeText(this, "Your location is" + longitude + " " + latitude, Toast.LENGTH_LONG);
        } else {
            Toast.makeText(this, "Unable to search by your location", Toast.LENGTH_LONG);
        }
//        Log.d("Position", "" + longitude + " " + latitude);
    }
}
