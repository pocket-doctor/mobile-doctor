package pocket.doctor.pocketdoctor.activities;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

import pocket.doctor.pocketdoctor.views.FormActivityViewManager;

/**
 * Created by jun on 12/21/16.
 * You can't understand this, Unless you're a genius!
 */
public class FormActivity extends AppCompatActivity {
    private final FormActivityViewManager formActivityManager = new FormActivityViewManager(this);

    protected String getTextOfId(Integer id) {
        return formActivityManager.getTextOfId(id);
    }

    protected void setTextOfId(String value, Integer id) {
        formActivityManager.setTextOfId(value, id);
    }

    protected void setTextFromKeyInIntent(String key, Integer id) {
        formActivityManager.setTextFromKeyInIntent(key, id);
    }

    protected void startActivity(Class<?> cls) {
        formActivityManager.startActivity(cls);
    }

    protected void setOnClickListenerForId(View.OnClickListener onClickListener, Integer id) {
        formActivityManager.setOnClickListenerForId(onClickListener, id);
    }

    protected void initButtons() {
    }
}
