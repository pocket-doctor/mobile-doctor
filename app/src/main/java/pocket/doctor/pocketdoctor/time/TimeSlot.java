package pocket.doctor.pocketdoctor.time;

import java.util.Date;

/**
 * Created by jun on 12/24/16.
 * You can't understand this, Unless you're a genius!
 */
public class TimeSlot {
    int id;
    private Date day;
    private String startTime;
    private String finishTime;
    boolean isReserved;

    public int getId() {
        return id;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public String getDay() {
        return day.toString();
    }
}
