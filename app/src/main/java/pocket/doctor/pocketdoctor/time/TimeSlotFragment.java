package pocket.doctor.pocketdoctor.time;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.fragments.FragmentKeys;

public class TimeSlotFragment extends Fragment {
    private OnListFragmentInteractionListener mListener;
    private TimeSlotGetter getter;
    private int id;
    private Date date;

    public TimeSlotFragment() {
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeslot_list, container, false);

        if (getArguments() != null) {
            id = getArguments().getInt(FragmentKeys.DOCTORS_SCHEDULE_ID_TAG.getKey());
            date = (Date) getArguments().getSerializable(FragmentKeys.DOCTORS_SCHEDULE_DAY_DATE_TAG.getKey());
            Log.d("GIBILI", id + " " + date);
        }

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            final MyTimeSlotRecyclerViewAdapter adapter = new MyTimeSlotRecyclerViewAdapter(mListener);
            recyclerView.setAdapter(adapter);
            getter = new TimeSlotGetter(adapter);
            Log.d("GIBILI", id + " " + date);
            getter.download(id, date);
        }
        return view;
    }

//    public void download(int id, Date date) {
//        getter.download(id, date);
//    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(TimeSlot item);
    }
}
