package pocket.doctor.pocketdoctor.time;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.scroll.InfiniteScrollRecyclerViewAdapter;
import pocket.doctor.pocketdoctor.time.TimeSlotFragment.OnListFragmentInteractionListener;
import pocket.doctor.pocketdoctor.views.RootViewManager;

public class MyTimeSlotRecyclerViewAdapter extends InfiniteScrollRecyclerViewAdapter<TimeSlot, MyTimeSlotRecyclerViewAdapter.ViewHolder> {

    private final OnListFragmentInteractionListener mListener;

    public MyTimeSlotRecyclerViewAdapter(OnListFragmentInteractionListener listener) {
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_timeslot, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.onBind(mValues.get(position));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private final RootViewManager manager;

        private void onBind(final TimeSlot timeSlot) {
            manager.setTextOfId(timeSlot.getStartTime(), R.id.start);
            manager.setTextOfId(timeSlot.getFinishTime(), R.id.finish);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onListFragmentInteraction(timeSlot);
                }
            });
        }

        public ViewHolder(View view) {
            super(view);
            mView = view;
            manager = new RootViewManager(mView);
        }
    }
}
