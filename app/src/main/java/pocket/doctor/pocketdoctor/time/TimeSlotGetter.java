package pocket.doctor.pocketdoctor.time;

import java.text.SimpleDateFormat;
import java.util.Date;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.scroll.InfiniteScrollAdapterAPI;
import pocket.doctor.pocketdoctor.scroll.InfiniteScrollGetter;
import pocket.doctor.pocketdoctor.scroll.PaginatedList;
import pocket.doctor.pocketdoctor.tools.Settings;
import retrofit2.Call;

/**
 * Created by jun on 1/8/17.
 * You can't understand this, Unless you're a genius!
 */
public class TimeSlotGetter extends InfiniteScrollGetter<TimeSlot> {
    private int id;
    private Date date;

    protected TimeSlotGetter(InfiniteScrollAdapterAPI<TimeSlot> dataNeededObject) {
        super(dataNeededObject);
    }

    public void download(int id, Date date) {
        this.date = date;
        this.id = id;
        dataNeededObject.resetDataSet();
        download();
    }

    private String getArgForm(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    @Override
    protected Call<PaginatedList<TimeSlot>> getCall() {

        return ServiceGenerator.getInstance().createService(OneDayScheduleAPI.class)
                .getSlots(id, getArgForm(date), Settings.getInstance().getAuthToken());
    }
}
