package pocket.doctor.pocketdoctor.time;

import pocket.doctor.pocketdoctor.scroll.PaginatedList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by jun on 1/8/17.
 * You can't understand this, Unless you're a genius!
 */
public interface OneDayScheduleAPI {
    @GET("schedule/slots/{id}/{date}")
    Call<PaginatedList<TimeSlot>> getSlots(@Path("id") int id, @Path("date") String date,
                                           @Header("Authorization") String authToken);
}
