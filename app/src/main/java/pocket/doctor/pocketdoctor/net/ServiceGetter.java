package pocket.doctor.pocketdoctor.net;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jun on 12/22/16.
 * You can't understand this, Unless you're a genius!
 */
public abstract class ServiceGetter<T> implements Callback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.code() < 400 && response.code() >= 100)
            onReceived(response.body());
        else
            onFail(new Throwable("Status code"));
        // Fuck You Amin :D :X
//        Love you Farnood >:D<
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onFail(t);
    }

    public abstract void onReceived(T content);

    public abstract void onFail(Throwable t);
}
