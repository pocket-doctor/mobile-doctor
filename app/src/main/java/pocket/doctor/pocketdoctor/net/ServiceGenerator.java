package pocket.doctor.pocketdoctor.net;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jun on 8/28/16.
 * You can't understand this, Unless you're a genius!
 */
public class ServiceGenerator {
    private final static ServiceGenerator INSTANCE = new ServiceGenerator();
    private final static int THRESHOLD = 6;

    private ServiceGenerator() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(THRESHOLD, TimeUnit.SECONDS).
                readTimeout(THRESHOLD, TimeUnit.SECONDS).addInterceptor(interceptor).build();


        retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    }

    public static ServiceGenerator getInstance() {
        return INSTANCE;
    }


    public static final String API_BASE_URL = "http://pocket-doctor.herokuapp.com/api/";

    private final Retrofit retrofit;

    public <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
