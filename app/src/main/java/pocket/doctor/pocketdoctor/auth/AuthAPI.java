package pocket.doctor.pocketdoctor.auth;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by jun on 12/22/16.
 * You can't understand this, Unless you're a genius!
 */
public interface AuthAPI {
    @POST("login/")
    Call<AuthToken> signIn(@Body SignInRequest request);
}
