package pocket.doctor.pocketdoctor.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.activities.ActivityKeys;
import pocket.doctor.pocketdoctor.activities.FormActivity;
import pocket.doctor.pocketdoctor.activities.MainActivity;
import pocket.doctor.pocketdoctor.join.JoinActivity;
import pocket.doctor.pocketdoctor.notification.NotificationReceiver;
import pocket.doctor.pocketdoctor.notification.NotificationService;
import pocket.doctor.pocketdoctor.tools.Settings;


public class LoginActivity extends FormActivity {
    private NotificationReceiver receiver = new NotificationReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        addPreferredValues();

        initButtons();
        shortPath();
        startNotificationService();
    }

    private void startNotificationService() {
        receiver.setPollingPeriod(this);
        startService(new Intent(this, NotificationService.class));
    }

    @Override
    protected void initButtons() {
        setOnClickListenerForId(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                join();
            }
        }, R.id.to_join_button);
        setOnClickListenerForId(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        }, R.id.login_button);
    }

    private void addPreferredValues() {
        setTextFromKeyInIntent(ActivityKeys.JOIN_DEFAULT_USERNAME.getKey(), R.id.username);
        setTextFromKeyInIntent(ActivityKeys.JOIN_DEFAULT_PASSWORD.getKey(), R.id.password);
    }

    private void login() {
        SignInRequest request = new SignInRequest(getTextOfId(R.id.username), getTextOfId(R.id.password));
        SignInRequester requester = new SignInRequester() {
            @Override
            public void onReceived(AuthToken content) {
                if (content != null) {
                    Settings.getInstance().setToken(content.getToken(), LoginActivity.this);
                    search();
                } else {
                    showErrors();
                }
            }

            @Override
            public void onFail(Throwable t) {
                showErrors();
            }
        };
        requester.signIn(request);
    }

    private void showErrors() {
        final TextView errorTextView = (TextView) findViewById(R.id.login_error);
        if (errorTextView != null)
            errorTextView.setVisibility(View.VISIBLE);
    }

    private void search() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void join() {
        Intent intent = new Intent(this, JoinActivity.class);
        String username = getTextOfId(R.id.username);
        if (username != null)
            intent.putExtra(ActivityKeys.JOIN_DEFAULT_USERNAME.getKey(), username);
        startActivity(intent);
    }

    private void shortPath() {
//        setTextOfId("gibiligibili", R.id.username);
//        setTextOfId("salam", R.id.password);
//        login();
    }
}
