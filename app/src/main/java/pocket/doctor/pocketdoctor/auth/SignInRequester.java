package pocket.doctor.pocketdoctor.auth;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.net.ServiceGetter;

/**
 * Created by jun on 12/22/16.
 * You can't understand this, Unless you're a genius!
 */
public abstract class SignInRequester extends ServiceGetter<AuthToken> {
    public void signIn(SignInRequest request) {
        ServiceGenerator.getInstance().createService(AuthAPI.class)
                .signIn(request).enqueue(this);
    }
}
