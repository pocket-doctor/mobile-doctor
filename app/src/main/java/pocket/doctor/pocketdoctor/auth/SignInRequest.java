package pocket.doctor.pocketdoctor.auth;

/**
 * Created by jun on 12/22/16.
 * You can't understand this, Unless you're a genius!
 */
public class SignInRequest {
    private String username;
    private String password;

    public SignInRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
