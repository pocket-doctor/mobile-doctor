package pocket.doctor.pocketdoctor.schedule;

import com.imanoweb.calendarview.DayDecorator;
import com.imanoweb.calendarview.DayView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jun on 1/8/17.
 * You can't understand this, Unless you're a genius!
 */
public class DayViewDecorator implements DayDecorator {
    public static List<DayDecorator>
    getInstance(Schedule schedule, int activeColor, int disabledColor) {
        List<DayDecorator> decorators = new ArrayList<>();
        decorators.add(new DayViewDecorator(schedule, activeColor, disabledColor));
        return decorators;
    }

    private final Schedule schedule;
    private final int activeColor;
    private final int disabledColor;

    public DayViewDecorator(Schedule schedule, int activeColor, int disabledColor) {
        this.schedule = schedule;
        this.activeColor = activeColor;
        this.disabledColor = disabledColor;
    }

    @Override
    public void decorate(DayView dayView) {
        int c = activeColor;
        if (!schedule.isWithinSchedule(dayView.getDate()))
            c = disabledColor;
        dayView.setTextColor(c);
    }
}
