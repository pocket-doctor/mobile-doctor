package pocket.doctor.pocketdoctor.schedule;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by jun on 12/24/16.
 * You can't understand this, Unless you're a genius!
 */
public class Schedule {
    private final List<Date> dates;

    public Schedule(List<Date> dates) {
        this.dates = dates;
    }

    public boolean isWithinSchedule(Date date) {
        for (Date d : dates)
            if (compare(date, d))
                return true;
        return false;
    }

    private boolean compare(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }
}
