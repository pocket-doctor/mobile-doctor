package pocket.doctor.pocketdoctor.schedule;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by jun on 1/7/17.
 * You can't understand this, Unless you're a genius!
 */
public interface ScheduleDaysAPI {
    @GET("schedule/slots/{id}/")
    Call<List<Date>> getDays(@Path("id") int id, @Header("Authorization") String authToken);
}
