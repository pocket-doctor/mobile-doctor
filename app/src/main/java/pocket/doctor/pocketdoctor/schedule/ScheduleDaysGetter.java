package pocket.doctor.pocketdoctor.schedule;

import java.util.Date;
import java.util.List;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.net.ServiceGetter;
import pocket.doctor.pocketdoctor.tools.Settings;

/**
 * Created by jun on 1/7/17.
 * You can't understand this, Unless you're a genius!
 */
public abstract class ScheduleDaysGetter extends ServiceGetter<List<Date>> {
//    public void getScheduleDays(Doctor doctor) {
//        getScheduleDays(doctor.getSchedule());
//    }

    public void getScheduleDays(int id) {
        ServiceGenerator.getInstance().createService(ScheduleDaysAPI.class)
                .getDays(id, Settings.getInstance().getAuthToken()).enqueue(this);
    }

    @Override
    public void onReceived(List<Date> content) {
        onReceived(new Schedule(content));
    }

    public abstract void onReceived(Schedule schedule);
}
