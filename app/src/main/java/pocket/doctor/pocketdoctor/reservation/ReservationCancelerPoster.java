package pocket.doctor.pocketdoctor.reservation;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.net.ServiceGetter;
import pocket.doctor.pocketdoctor.tools.Settings;

/**
 * Created by jun on 2/3/17.
 * You can't understand this, Unless you're a genius!
 */
public abstract class ReservationCancelerPoster extends ServiceGetter<Void> {
    private final ReservationType reservationType;

    public ReservationCancelerPoster(ReservationType reservationType) {
        this.reservationType = reservationType;
    }

    public void cancel(int id) {
        ServiceGenerator.getInstance().createService(ReserveAPI.class).cancel(id,
                reservationType.getCancelURL(), Settings.getInstance().getAuthToken()).enqueue(this);
    }
}
