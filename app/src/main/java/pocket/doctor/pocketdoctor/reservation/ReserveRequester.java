package pocket.doctor.pocketdoctor.reservation;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.net.ServiceGetter;
import pocket.doctor.pocketdoctor.tools.Settings;

/**
 * Created by jun on 1/8/17.
 * You can't understand this, Unless you're a genius!
 */
public abstract class ReserveRequester extends ServiceGetter<Void> {
    public void request(int id) {
        ServiceGenerator.getInstance().createService(ReserveAPI.class).reserve(id,
                Settings.getInstance().getAuthToken()).enqueue(this);
    }
}
