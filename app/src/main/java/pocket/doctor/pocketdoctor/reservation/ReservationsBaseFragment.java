package pocket.doctor.pocketdoctor.reservation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.fragments.TabAdapter;

/**
 * Created by jun on 11/8/16.
 * You can't understand this, Unless you're a genius!
 */
public class ReservationsBaseFragment extends Fragment {
    private ViewPager mPager;
    private TabLayout mTabs;

    public ReservationsBaseFragment() {
    }

    private void setViewPager(final ViewPager pager) {
        TabAdapter adapter = new TabAdapter(getChildFragmentManager());
        adapter.addPage(ReservationFragment.newInstance(ReservationType.ACCEPTED), "Appointments");
        adapter.addPage(ReservationFragment.newInstance(ReservationType.REJECTED), "Rejected Reservations");
        adapter.addPage(ReservationFragment.newInstance(ReservationType.PENDING), "Pending Reservations");
        pager.setAdapter(adapter);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_reservation, container, false);
        mPager = (ViewPager) view.findViewById(R.id.pager);
        mTabs = (TabLayout) view.findViewById(R.id.tabs);

        assert mTabs != null;
        assert mPager != null;


        setViewPager(mPager);
        mTabs.setupWithViewPager(mPager);
        return view;
    }
}
