package pocket.doctor.pocketdoctor.reservation;

import java.io.IOException;

import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.scroll.InfiniteScrollAdapterAPI;
import pocket.doctor.pocketdoctor.scroll.InfiniteScrollGetter;
import pocket.doctor.pocketdoctor.scroll.PaginatedList;
import pocket.doctor.pocketdoctor.tools.Settings;
import retrofit2.Call;

/**
 * Created by jun on 1/8/17.
 * You can't understand this, Unless you're a genius!
 */
public class ReservationGetter extends InfiniteScrollGetter<Reservation> {
    private final ReservationType reservationType;

    public ReservationGetter(InfiniteScrollAdapterAPI<Reservation> dataNeededObject,
                             ReservationType reservationType) {
        super(dataNeededObject);
        this.reservationType = reservationType;
    }

    @Override
    public void download() {
        this.dataNeededObject.resetDataSet();
        super.download();
    }

    @Override
    protected Call<PaginatedList<Reservation>> getCall() {
        return ServiceGenerator.getInstance().createService(ReserveAPI.class)
                .getReservation(reservationType.getListURL(), Settings.getInstance().getAuthToken());
    }

    public int getFirstReservationId() throws IOException {
        PaginatedList<Reservation> list = getCall().execute().body();
        if (list != null && list.getResults() != null && !list.getResults().isEmpty())
            list.getResults().get(0).getSlotID();
        return -1;
    }
}
