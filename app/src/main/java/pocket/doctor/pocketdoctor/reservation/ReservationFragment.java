package pocket.doctor.pocketdoctor.reservation;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.fragments.FragmentKeys;

public class ReservationFragment extends Fragment {

    private ReservationType reservationType;
    private ReservationGetter getter;
    private OnCancelListener mListener;

    public ReservationFragment() {
    }

    public static ReservationFragment newInstance(ReservationType requestsType) {
        ReservationFragment fragment = new ReservationFragment();
        Bundle args = new Bundle();
        args.putInt(FragmentKeys.RESERVATION_TYPE_ID_TAG.getKey(), requestsType.ordinal());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
            reservationType = ReservationType.values()[getArguments()
                    .getInt(FragmentKeys.RESERVATION_TYPE_ID_TAG.getKey())];
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reservation_list, container, false);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            final MyReservationRecyclerViewAdapter adapter =
                    new MyReservationRecyclerViewAdapter(reservationType, mListener);
            getter = new ReservationGetter(adapter, reservationType);
            recyclerView.setAdapter(adapter);
            download();
        }
        return view;
    }

    private void download() {
        getter.download();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCancelListener) {
            mListener = (OnCancelListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDoctorsListFragmentClickListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnCancelListener {
        void onCancelClicked(Reservation reservation, ReservationType type);
    }
}
