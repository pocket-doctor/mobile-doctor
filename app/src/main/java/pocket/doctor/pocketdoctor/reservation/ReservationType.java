package pocket.doctor.pocketdoctor.reservation;

/**
 * Created by jun on 1/8/17.
 * You can't understand this, Unless you're a genius!
 */
public enum ReservationType {
    PENDING("requests", "request"),
    ACCEPTED("reserves", "reserve"),
    REJECTED("failed-requests", null);

    private final String getListURL;
    private final String cancelURL;

    ReservationType(String getListURL, String cancelURL) {
        this.getListURL = getListURL;
        this.cancelURL = cancelURL;
    }

    public String getListURL() {
        return getListURL;
    }

    public String getCancelURL() {
        return cancelURL;
    }
}
