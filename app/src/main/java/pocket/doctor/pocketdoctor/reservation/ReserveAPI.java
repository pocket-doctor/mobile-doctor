package pocket.doctor.pocketdoctor.reservation;

import pocket.doctor.pocketdoctor.scroll.PaginatedList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by jun on 1/8/17.
 * You can't understand this, Unless you're a genius!
 */
public interface ReserveAPI {
    @POST("schedule/{id}/request/")
    Call<Void> reserve(@Path("id") int id, @Header("Authorization") String authToken);

    @GET("schedule/{url}/")
    Call<PaginatedList<Reservation>> getReservation(@Path("url") String url,
                                                    @Header("Authorization") String authToken);

    @POST("schedule/{id}/cancel-{url}/")
    Call<Void> cancel(@Path("id") int id, @Path("url") String url,
                      @Header("Authorization") String authToken);
}
