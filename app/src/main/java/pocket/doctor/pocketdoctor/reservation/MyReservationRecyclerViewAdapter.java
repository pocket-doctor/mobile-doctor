package pocket.doctor.pocketdoctor.reservation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.reservation.ReservationFragment.OnCancelListener;
import pocket.doctor.pocketdoctor.scroll.InfiniteScrollRecyclerViewAdapter;
import pocket.doctor.pocketdoctor.views.RootViewManager;

public class MyReservationRecyclerViewAdapter extends
        InfiniteScrollRecyclerViewAdapter<Reservation, MyReservationRecyclerViewAdapter.ViewHolder> {

    private final ReservationType type;
    private final OnCancelListener listener;

    public MyReservationRecyclerViewAdapter(ReservationType type, OnCancelListener listener) {
        this.type = type;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_reservation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.onBind(mValues.get(position), position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private final RootViewManager manager;

        public void onBind(final Reservation reservation, final int position) {
//            TODO: We need the backend to send us the doctors name
            manager.setTextOfId("Doctor:", R.id.doctor);

            manager.setTextOfId(reservation.getDay(), R.id.doctor);
            manager.setTextOfId(reservation.getStart(), R.id.start);
            manager.setTextOfId(reservation.getFinish(), R.id.finish);

            if (type == ReservationType.REJECTED)
                manager.findViewById(R.id.cancel).setVisibility(View.GONE);

            manager.setOnClickListenerForId(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCancelClicked(reservation, type);
                    MyReservationRecyclerViewAdapter.this.mValues.remove(position);
                    MyReservationRecyclerViewAdapter.this.notifyDataSetChanged();
                }
            }, R.id.cancel);
        }

        public ViewHolder(View view) {
            super(view);
            mView = view;
            manager = new RootViewManager(mView);
        }
    }
}
