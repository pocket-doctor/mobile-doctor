package pocket.doctor.pocketdoctor.reservation;

import java.util.Date;

import pocket.doctor.pocketdoctor.time.TimeSlot;

/**
 * Created by jun on 1/8/17.
 * You can't understand this, Unless you're a genius!
 */
public class Reservation {
    private final Date created;
    private final TimeSlot slot;

    public Reservation(Date created, TimeSlot slot) {
        this.created = created;
        this.slot = slot;
    }

    public int getSlotID() {
        return slot.getId();
    }

    public Date getCreated() {
        return created;
    }

    public String getDay() {
//  TODO: Tof!
        if (slot != null)
            return slot.getDay();
        return "Date & Time not Available";
    }

    public String getStart() {
        //  TODO: Tof!
        if (slot != null)
            return slot.getStartTime();
        return "Date & Time not Available";
    }

    public String getFinish() {
//  TODO: Tof!
        if (slot != null)
            return slot.getFinishTime();
        return "Date & Time not Available";
    }
}
