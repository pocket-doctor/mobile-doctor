package pocket.doctor.pocketdoctor.scroll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jun on 8/28/16.
 * You can't understand this, Unless you're a genius!
 */
public abstract class InfiniteScrollGetter<T> implements Callback<PaginatedList<T>> {
    protected final InfiniteScrollAdapterAPI<T> dataNeededObject;

    private Call<PaginatedList<T>> call;

    protected InfiniteScrollGetter(InfiniteScrollAdapterAPI<T> dataNeededObject) {
        this.dataNeededObject = dataNeededObject;
    }

    public void download() {
        call = getCall();
        call.enqueue(this);
    }

    public void cancel() {
        if (call != null)
            call.cancel();
    }


    protected abstract Call<PaginatedList<T>> getCall();

    @Override
    public void onResponse(Call<PaginatedList<T>> call, Response<PaginatedList<T>> response) {
        dataNeededObject.updateDataSet(response.body());
        call = null;
    }

    @Override
    public void onFailure(Call<PaginatedList<T>> call, Throwable t) {
        t.printStackTrace();
        download();
    }
}

