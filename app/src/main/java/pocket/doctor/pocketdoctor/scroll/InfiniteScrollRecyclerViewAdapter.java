package pocket.doctor.pocketdoctor.scroll;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jun on 10/23/16.
 * You can't understand this, Unless you're a genius!
 */
public abstract class InfiniteScrollRecyclerViewAdapter<Type, ViewHolderType extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<ViewHolderType> implements InfiniteScrollAdapterAPI<Type> {

    protected final List<Type> mValues = new ArrayList<>();


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void updateDataSet(PaginatedList<Type> data) {
        if (data != null) {
            mValues.addAll(data.getResults());
            notifyDataSetChanged();
        }
    }

    @Override
    public void resetDataSet() {
        mValues.clear();
    }
}
