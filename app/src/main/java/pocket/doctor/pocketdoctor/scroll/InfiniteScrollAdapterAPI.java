package pocket.doctor.pocketdoctor.scroll;

/**
 * Created by jun on 10/25/16.
 * You can't understand this, Unless you're a genius!
 */
public interface InfiniteScrollAdapterAPI<Type> {
    void updateDataSet(PaginatedList<Type> data);

    void resetDataSet();
}
