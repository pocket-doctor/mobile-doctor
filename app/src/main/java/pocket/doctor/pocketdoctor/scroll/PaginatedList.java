package pocket.doctor.pocketdoctor.scroll;

import java.util.List;

/**
 * Created by jun on 10/25/16.
 * You can't understand this, Unless you're a genius!
 */
public class PaginatedList<T> {
    private String next;
    List<T> results;

    public void setResults(List<T> results) {
        this.results = results;
    }

    public String getNext() {
        return next;
    }

    public List<T> getResults() {
        return results;
    }

    @Override
    public String toString() {
        String out = String.format("[ next: %s , result:[", next);
        for (T t : results)
            out += String.format(" %s,", t);
        out += "] ] ";
        return out;
    }
}
