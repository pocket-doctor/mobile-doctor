package pocket.doctor.pocketdoctor.scroll;

/**
 * Created by jun on 12/23/16.
 * You can't understand this, Unless you're a genius!
 */
public abstract class SearchScrollGetter<QueryType, ResultType> extends InfiniteScrollGetter<ResultType> {
    private QueryType query;

    protected SearchScrollGetter(InfiniteScrollAdapterAPI<ResultType> dataNeededObject) {
        super(dataNeededObject);
    }

    public void setQuery(QueryType query) {
        this.query = query;
    }

    public QueryType getQuery() {
        return query;
    }
}
