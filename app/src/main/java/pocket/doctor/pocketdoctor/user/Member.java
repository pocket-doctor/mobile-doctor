package pocket.doctor.pocketdoctor.user;

/**
 * Created by jun on 12/21/16.
 * You can't understand this, Unless you're a genius!
 */
public class Member {
    protected String melliCode;
    protected String phoneNumber;

    public Member(String melliCode, String phoneNumber) {
        this.melliCode = melliCode;
        this.phoneNumber = phoneNumber;
    }

    public Member() {
    }

    public void setMelliCode(String melliCode) {
        this.melliCode = melliCode;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMelliCode() {
        return melliCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
