package pocket.doctor.pocketdoctor.user;

/**
 * Created by jun on 12/21/16.
 * You can't understand this, Unless you're a genius!
 */
public class User {
    protected Member member;
    protected String username;
    protected String firstName;
    protected String lastName;
    protected String email;

    public User(Member member, String username, String firstName, String lastName, String email) {
        this.member = member;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public User() {
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMelliCode(String melliCode) {
        if (member == null)
            this.member = new Member();
        this.member.setMelliCode(melliCode);
    }

    public void setPhoneNumber(String phoneNumber) {
        if (member == null)
            this.member = new Member();
        member.setPhoneNumber(phoneNumber);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMelliCode() {
        if (member != null)
            return member.getMelliCode();
        return "No information";
    }

    public String getPhoneNumber() {
        if (member != null)
            return member.getPhoneNumber();
        return "No information";
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }
}
