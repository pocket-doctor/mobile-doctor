package pocket.doctor.pocketdoctor.advertisement;

import java.util.List;

import pocket.doctor.pocketdoctor.doctors.Doctor;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by jun on 2/4/17.
 * You can't understand this, Unless you're a genius!
 */
public interface AdvertisementAPI {
    @GET("advertisement/show/")
    Call<List<Doctor>> getDoctors(@Header("Authorization") String authToken);
}
