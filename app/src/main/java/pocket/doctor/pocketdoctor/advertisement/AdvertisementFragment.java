package pocket.doctor.pocketdoctor.advertisement;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pocket.doctor.pocketdoctor.R;
import pocket.doctor.pocketdoctor.doctors.DoctorFragment.OnDoctorsListFragmentClickListener;
import pocket.doctor.pocketdoctor.doctors.MyDoctorRecyclerViewAdapter;

/**
 * Created by jun on 2/4/17.
 * You can't understand this, Unless you're a genius!
 */
public class AdvertisementFragment extends Fragment {

    private AdvertisementGetter getter;
    private OnDoctorsListFragmentClickListener mListener;
    private MyDoctorRecyclerViewAdapter adapter;

    public AdvertisementFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctor_list, container, false);
        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new MyDoctorRecyclerViewAdapter(mListener);
        recyclerView.setAdapter(adapter);
        getter = new AdvertisementGetter(adapter);
        getter.download();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDoctorsListFragmentClickListener) {
            mListener = (OnDoctorsListFragmentClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDoctorsListFragmentClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
