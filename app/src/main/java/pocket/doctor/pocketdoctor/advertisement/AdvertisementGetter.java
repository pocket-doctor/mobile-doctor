package pocket.doctor.pocketdoctor.advertisement;

import java.util.List;

import pocket.doctor.pocketdoctor.doctors.Doctor;
import pocket.doctor.pocketdoctor.net.ServiceGenerator;
import pocket.doctor.pocketdoctor.net.ServiceGetter;
import pocket.doctor.pocketdoctor.scroll.InfiniteScrollAdapterAPI;
import pocket.doctor.pocketdoctor.scroll.PaginatedList;
import pocket.doctor.pocketdoctor.tools.Settings;

/**
 * Created by jun on 2/4/17.
 * You can't understand this, Unless you're a genius!
 */
public class AdvertisementGetter extends ServiceGetter<List<Doctor>> {
    private final InfiniteScrollAdapterAPI<Doctor> dataNeededObject;

    protected AdvertisementGetter(InfiniteScrollAdapterAPI<Doctor> dataNeededObject) {
        this.dataNeededObject = dataNeededObject;
    }

    void download() {
        ServiceGenerator.getInstance().createService(AdvertisementAPI.class).getDoctors(
                Settings.getInstance().getAuthToken()).enqueue(this);
    }

    @Override
    public void onReceived(List<Doctor> content) {
        PaginatedList<Doctor> list = new PaginatedList<>();
        list.setResults(content);
        dataNeededObject.updateDataSet(list);
    }

    @Override
    public void onFail(Throwable t) {
        download();
    }
}
